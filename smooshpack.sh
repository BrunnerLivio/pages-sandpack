#!/bin/sh

# Download the package tarball
wget $(npm view smooshpack@0.0.62 dist.tarball)

# Extract just the `sandpack` folder which contains the pre-built assets
tar -xzf smooshpack-0.0.62.tgz package/sandpack

# Move package/sandpack to ./public
mv ./package/sandpack/* ./public
