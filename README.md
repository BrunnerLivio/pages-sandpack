![Build Status](https://gitlab.com/phikai/pages-sandpack/badges/master/pipeline.svg)

---

Example project to build and deploy the [Codesandbox Sandpack](https://www.npmjs.com/package/smooshpack) scripts for Web IDE Live Preview.

### Why???

This might be a viable path for instances to self-host the required dependencies to make Live Preview work in instances that don't want to serve this from GitLab.com.

---

## Deployment

This project deploys the sandpack dependencies via GitLab CI and hosts them in GitLab Pages

```
image: node:latest

pages:
  stage: deploy
  script:
  - ./smooshpack.sh
  artifacts:
    paths:
    - public
  only:
  - master
```

```
#!/bin/sh

# Download the package tarball
wget $(npm view smooshpack@0.0.62 dist.tarball)

# Extract just the `sandpack` folder which contains the pre-built assets
tar -xzf smooshpack-0.0.62.tgz package/sandpack

# Move package/sandpack to ./public
mv ./package/sandpack/* ./public
```

The above script extracts the appropriate contents from the `smooshpack` package and moves them to the `public/` directory to be deployed to GitLab Pages.

## Custom Domain

Currently this project is running on a custom domain so that the files are served from a root domain. It can be accessed at `https://sandpack.thinkonezero.com`.

Modifying the [bundler_url](https://gitlab.com/gitlab-org/gitlab/blob/master/app/models/application_setting_implementation.rb#L357) and pointing it to the pages domain allows the assets to be served from that domain.
